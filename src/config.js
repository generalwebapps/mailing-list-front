let baseUrl = '';

if (process.env.NODE_ENV === 'production') {
    baseUrl = 'https://localhost:7211/api'
} else {
    baseUrl = 'https://localhost:7211/api'
}

export const apiHost = baseUrl;

export const mailSuscriberApi = {
    addSuscriber: `${apiHost}/email-suscriber`,
    getSuscribers: `${apiHost}/email-suscriber`
}