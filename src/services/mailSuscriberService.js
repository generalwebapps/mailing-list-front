import axios from "axios";
import { mailSuscriberApi } from '../config'

export default {

    async getSuscribers(suscriberFilters) {

        let url = mailSuscriberApi.getSuscribers
        let config = {
            params: suscriberFilters,
        };

        try {
            const axiosRespond = await axios.get(url, config);
            const payload = axiosRespond.data;

            return payload;

        } catch (e) {
            console.log(e);
            throw e;
        }

    },
    async addSuscriber(newSuscriber) {
        debugger;
        let url = mailSuscriberApi.addSuscriber

        try {
            const axiosRespond = await axios.post(url, newSuscriber);
            const payload = axiosRespond.data;

            return payload;

        } catch (e) {
            console.log(e);
            throw e;
        }

    },

};