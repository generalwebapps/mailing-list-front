import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios';
import { apiHost } from './config'


axios.defaults.baseURL = apiHost
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')